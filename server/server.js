console.log('Node js!');
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
app.use(express.json());

mongoose.connect('mongodb://mongo/mongo_test', {useNewUrlParser: true}, function (err) {
    if (err) {
        console.log('Connection Error: ', err);
        throw err;
    }
    console.log('Db connected');
});
app.use('/resources', express.static(__dirname + '/resources'));
// Cors configuration
app.options('*', cors());
app.use(cors({
    origin: ['http://app.node-test.com', 'http://node-test.com', 'http://localhost:4200', 'http://localhost:3000', 'http://localhost:8080'],
    credentials: true
}));
const apiRoute = require('./route')(express.Router());
app.use('/api', apiRoute);
app.use('/', apiRoute);
app.listen(8080);
module.exports = {app};
