const mongoose = require('mongoose');
const wordSchema = new mongoose.Schema({
    word: String,
    leng: String,
    translate: [{type: mongoose.Schema.Types.ObjectId, ref: 'Word'}]
});
module.exports = mongoose.model('Word', wordSchema);
