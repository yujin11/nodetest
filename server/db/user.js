const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
    fName: String,
    lName: String,
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: null
    }
}, {
    // timestamps: true
});

module.exports = mongoose.model('User', userSchema);
