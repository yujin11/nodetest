const secret = '6Lcl_KwUAAAAANCd3cRwGQLaT2tfhuiHu0t-1Y57';

const asyncRequest = require('request-promise');

checkCaptcha = async (req, response) => {
    const token = req.body.response;
    const requestData = {
        secret,
        response: token,
    };
    try {
        const data = await asyncRequest('https://www.google.com/recaptcha/api/siteverify', {
            method: 'GET',
            qs: requestData,
            json: true
        }); 
        response.status(200).json(data);
    } catch (e) {
        throw e;
    }

}

module.exports = checkCaptcha;
