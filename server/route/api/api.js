const data = [
    {
        id: 0,
        name: 'Data 1'
    }, {
        id: 1,
        name: 'Bla-bla-bla.'
    }, {
        id: 2,
        name: 'Data 715'
    }, {
        id: 3,
        name: 'Data 33'
    },
];
module.exports = async (req, resp) => {
    resp.status(200).json(data);
};
