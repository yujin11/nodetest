const User = require('../../db/user');
const step = 10;
addUser = async (req, res) => {
    const data = req.body;
    if (!data)
        res.status(500).send('No data available');
    const newUser = new User({
        fName: data.fName,
        lName: data.lName,
    }).save((error, user) => {
        if (error) {
            res.status(500).send("Can't save user.");
            throw error;
        }
        console.log('User was successfully added.', user._id);
        res.status(200).send(String(user._id));
    })
};

updateUser = async (req, resp) => {
    let updatedUser = req.body;
    updatedUser.updated = new Date();
    User.update({_id: req.params.id}, updatedUser, function (err, user) {
        if (err) {
            resp.sendStatus(500);
            throw err;
        }
        resp.status(200).json(user);
    });
}

getById = async (req, resp) => {
    const id = req.params.id;
    if (!id) {
        resp.sendStatus(500);
    }
    try {
        User.findById(id, (err, data) => {
            if (err) {
                resp.sendStatus(500);
                console.log('Error - Find User by Id: ', err);
            }
            resp.status(200).json(data);
        });
    } catch (e) {
        console.log('Error - Find User by Id: ', e);
    }
};

getList = async (req, resp) => {
    console.log('Get By List.');
    let sorting = {};
    const data = req.query;
    const isAsc = (data.hasOwnProperty('isAsc') && data.isAsc != 'true') ? -1 : 1;
    const page = (data.page) ? data.page : 0;
    (data.sortingBy) ? sorting[data.sortingBy] = isAsc : sorting['fName'] = isAsc;
    User.find({}, null, {sort: sorting, skip: page * step, limit: step}, (err, data) => {
        if (err) {
            console.log('Get User List Errorn: ', err);
            res.status(500).send('User Find errror.');
            throw err;
        } else {
            resp.status(200).json(data);
        }
    });
};

getCount = async (request, response) => {
    User.countDocuments({}, (err, count) => {
        if (err) {
            console.log('User count error.');
            response.status(500).send('User count error.');
            throw err;
        }
        response.status(200).send(String(count));
    });
};

deleteUser = async (request, response) => {
    const body = request;
    if (!body.query.id)
        response.status(500).send('No data available');
    User.findByIdAndDelete(body.query.id, (error, data) => {
        if (error) {
            console.log("Can't delete user: ", body.query.id);
            res.status(500).send('No data available');
            throw error;
        }
        response.status(200).end();
    });
};

const userApi = {
    addUser,
    updateUser,
    getList,
    getById,
    deleteUser,
    getCount
};

module.exports = userApi;
