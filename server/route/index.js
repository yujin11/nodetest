var api = require('./api/api');
const user = require('./api/user');
const captcha = require('./api/recaptcha');
module.exports = (router) => {
    router.get('/data', api);
    router.get('/user', user.getList);
    router.get('/user/list', user.getCount);
    router.get('/user/:id', user.getById);
    router.post('/user', user.addUser);
    router.put('/user/:id', user.updateUser);
    router.delete('/user', user.deleteUser);
    router.post('/recaptcha', captcha);
    return router;
}
