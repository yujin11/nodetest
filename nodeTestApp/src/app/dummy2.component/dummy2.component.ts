import {Component} from "@angular/core";

@Component({
  selector:'app-2dummy',
  templateUrl: './dummy2.component.html'
})
export class Dummy2Component {

  title: string;

  constructor(){
    this.title = 'Dummy 2 title'
  }

}
