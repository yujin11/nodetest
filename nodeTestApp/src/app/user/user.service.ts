import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User, UserRequest} from "./models/User";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(protected http: HttpClient) {

  }

  // url = 'http://app.node-test.com/api/user';
  url = 'http://localhost:8080/api/user';

  public getUserList(isAsc?: boolean, sortingField?: string, page?: number): Observable<User[]> {
    if (!page)
      page = 0;
    const link = this.url + "?isAsc=" + isAsc + "&sortingBy=" + sortingField + "&page=" + page;
    return this.http.get<User[]>(link);
  }

  public getUsersCount(): Observable<number> {
    return this.http.get<number>(this.url + '/list');
  }

  public setUser(user: UserRequest): Observable<string> {
    return this.http.post<string>(this.url, user);
  }

  public deleteUser(id: string): Observable<any> {
    return this.http.delete(this.url, {params: {id: id}});
  }

  public updateUser(user: UserRequest, userId: string): Observable<User> {
    return this.http.put<User>(this.url + '/' + userId, user);
  }

  public getOneById(userId: string): Observable<User> {
    return this.http.get<User>(this.url + '/' + userId);
  }

  public getSomeData() {
    return 'getSomeData';
  }
}
