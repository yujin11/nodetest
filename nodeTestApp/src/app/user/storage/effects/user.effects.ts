import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects'
import {UserService} from "../../user.service";
import * as fromActions from '../actions';
import {catchError, map, mergeMap, tap} from "rxjs/operators";
import {Action} from "@ngrx/store";
import {EMPTY} from "rxjs";

@Injectable()
export class UserEffects {

  constructor(private actions$: Actions<Action>, private userService: UserService) {
  }

  loadUsers$ = createEffect(() => this.actions$.pipe(
    ofType(fromActions.getUserListAction),
    mergeMap(() => this.userService.getUserList().pipe(
      map(list => (fromActions.userListLoadedAction({list: list}))),
      catchError(() => EMPTY)
    ))
  ));
}
