import {createAction, props} from "@ngrx/store";
import {User} from "../../models/User";

export const getUserListAction = createAction('[User Component] get user list action');
export const getUserAction = createAction('[User Component] get user action', props<{ _id: string }>());
export const userListLoadingAction = createAction('[User Component] user list loading action');
export const userListLoadedAction = createAction('[User Component] user list loaded action', props<{ list: User[] }>());
export const userListLoadErrorAction = createAction('[User Component] user list load error action', props<{ error: string }>());
