import {UserState} from "./user.reducer";

export * from './user.reducer';

export interface UserModuleState {
  user: UserState
}
