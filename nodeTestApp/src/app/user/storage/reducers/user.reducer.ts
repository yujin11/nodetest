import {User} from "../../models/User";
import {createReducer, createSelector, on} from "@ngrx/store";
import * as fromActions from '../actions';
import {UserModuleState} from "./index";

export interface UserState {
  list: User[],
  loaded: boolean,
  loading: boolean,
  loadError: boolean
};

const defaultUserState: UserState = {
  list: [],
  loaded: false,
  loading: false,
  loadError: false
};

export const userReducer = createReducer(defaultUserState,
  on(fromActions.getUserListAction, state => state),
  on(fromActions.userListLoadedAction, (state, {list}) => {
    return {
      list: list,
      loaded: true,
      loading: false,
      loadError: false
    }
  }),
  on(fromActions.userListLoadingAction, state => ({...state, loadError: false, loading: true, loaded: false})),
  on(fromActions.userListLoadErrorAction, (state) => ({
    ...state,
    loaded: false,
    loading: false,
    loadError: true
  })));

const getUserById = (state: UserModuleState, id: string) => state.user.list.filter((user: User) => user._id == id);
const userListLoaded = (state: UserModuleState) => state.user.list;
const userListLoading = (state: UserModuleState) => state.user.loading;
const userListLoadingError = (state: UserModuleState) => state.user.loadError;

export const userListSelector = createSelector(userListLoaded, list => list);
export const userByIdSelector = createSelector(getUserById, user => (user.length > 0) ? user[0] : null);
