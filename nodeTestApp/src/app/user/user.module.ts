import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {UserComponent} from "./components/user.component/user.component";
import {RouterModule} from "@angular/router";
import {routes} from "./user.routes";
import {AddUserComponent} from "./components/add.user.component/add.user.component";
import {ReactiveFormsModule} from "@angular/forms";
import {ShareModule} from "../share.module";
import {EffectsModule} from "@ngrx/effects";
import {UserEffects} from './storage/effects';
import {StoreModule} from "@ngrx/store";
import * as fromStorage from './storage'

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    ShareModule,
    EffectsModule.forFeature([UserEffects]),
    StoreModule.forFeature('user', fromStorage.userReducer)
  ],
  declarations: [UserComponent, AddUserComponent]
})
export class UserModule {

}
