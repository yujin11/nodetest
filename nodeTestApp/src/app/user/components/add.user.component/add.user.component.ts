import {Component} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../user/user.service";
import {User, UserRequest} from "../../../user/models/User";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-add-user',
  templateUrl: './add.user.component.html'
})
export class AddUserComponent {
  addUserForm: FormGroup = new FormGroup({});
  isEdit: boolean = false;
  userId: string;

  constructor(
    protected userService: UserService,
    protected router: Router,
    protected route: ActivatedRoute
  ) {
    this.addUserForm = new FormGroup({
      fName: new FormControl('', {validators: Validators.required}),
      lName: new FormControl('', {validators: Validators.required}),
    })
    this.userId = this.route.snapshot.paramMap.get("id");
    this.isEdit = (!!this.userId);
    if (this.isEdit) {
      this.userService.getOneById(this.userId).subscribe((data: User) => {
        const user = data;
        this.addUserForm.get('fName').setValue(user.fName);
        this.addUserForm.get('lName').setValue(user.lName);
      });
    }
  }

  submitData() {
    for (let key in this.addUserForm.controls) {
      this.addUserForm.controls[key].markAsTouched();
    }
    if (this.addUserForm.invalid)
      return;
    const user: UserRequest = {
      fName: this.addUserForm.get('fName').value,
      lName: this.addUserForm.get('lName').value
    };
    if (!this.isEdit) {
      this.addUser(user);
    } else {
      this.updateUser(user);
    }
  }

  private updateUser(user: UserRequest) {
    this.userService.updateUser(user, this.userId).subscribe(result => {
      this.router.navigate(['/user']);
    }, error1 => {
      console.log('Error in setUser request.');
    })
  }

  private addUser(user: UserRequest) {
    this.userService.setUser(user).subscribe(result => {
      this.router.navigate(['/user']);
    }, error1 => {
      console.log('Error in setUser request.');
    })
  }

}
