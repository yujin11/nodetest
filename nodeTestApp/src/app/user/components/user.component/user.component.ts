import {Component, OnInit} from "@angular/core";
import {UserService} from "../../../user/user.service";
import {User} from "../../../user/models/User";
import {Observable} from "rxjs";
import {ApiService} from "../../../services/api.service";
import {select, Store} from "@ngrx/store";
import * as fromStorage from '../../storage';

@Component({
  selector: 'app-user',
  templateUrl: "./user.component.html"
})
export class UserComponent implements OnInit {

  userList$: Observable<User[]> = this.store.pipe(select(fromStorage.userListSelector));

  sortingModel;

  isAsc: boolean = true;
  public selectedSortingField: string = '';
  currentPage: number = 1;
  pageList: number;

  constructor(
    protected userService: UserService,
    protected apiService: ApiService,
    protected store: Store<fromStorage.UserModuleState>
  ) {
    this.sortingModel = {
      name: "fName",
      lName: "lName",
      date: "created"
    };
    this.selectedSortingField = this.sortingModel.name;
    // this.getList();
    this.userService.getUsersCount().subscribe(data => {
      this.pageList = data;
    })
  }

  private getList() {
    this.userList$ = this.userService.getUserList(this.isAsc, this.selectedSortingField, this.currentPage - 1);
  }

  deleteUser(userId: string) {
    this.userService.deleteUser(userId).subscribe(data => {
      this.store.dispatch(fromStorage.getUserListAction());
    })
  }

  sort(field: string) {
    if (field == this.selectedSortingField) {
      this.isAsc = !this.isAsc;
    } else {
      this.isAsc = true;
    }
    this.selectedSortingField = field;
    this.store.dispatch(fromStorage.getUserListAction());
  }

  ngOnInit(): void {
    this.store.dispatch(fromStorage.getUserListAction());
  }

}
