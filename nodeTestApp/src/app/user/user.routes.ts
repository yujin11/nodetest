import {Routes} from "@angular/router";
import {UserComponent} from "./components/user.component/user.component";
import {AddUserComponent} from "./components/add.user.component/add.user.component";

export const routes: Routes = [
  {
    path: '',
    component: UserComponent
  },
  {
    path: 'add',
    component: AddUserComponent
  },
  {
    path: 'edit/:id',
    component: AddUserComponent
  }
];
