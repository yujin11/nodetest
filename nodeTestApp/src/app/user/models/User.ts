export interface User {
  _id: string;
  fName: string;
  lName: string;
  created: Date;
}

export interface UserRequest {
  fName: string;
  lName: string;
}
