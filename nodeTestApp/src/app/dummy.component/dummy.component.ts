import {Component} from "@angular/core";

@Component({
  selector:'app-dummy',
  templateUrl: './dummy.component.html'
})
export class DummyComponent {

  title: string;

  constructor(){
    this.title = 'Dummy title'
  }

}
