import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainComponent} from "./main.component/main.component";

const routes: Routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: 'user',
    loadChildren: () => import('./user/user.module').then(mod => mod.UserModule)
  },
  {
    path: 'book',
    loadChildren: () => import('./books/book.module').then(mod => mod.BookModule)
  },
  {
    path: 'author',
    loadChildren: () => import('./author/author.module').then(mod => mod.AuthorModule)
  },
  {
    path: 'cars',
    loadChildren: () => import('./car/car.module').then(mod => mod.CarModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
