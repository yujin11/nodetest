import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(protected http: HttpClient) {

  }

  getData(): Observable<any> {
    return this.http.get('http://localhost:8080/api/data');
  }

  sendReCaptcha(response) {
    const secret = '6Lcl_KwUAAAAANCd3cRwGQLaT2tfhuiHu0t-1Y57';
    return this.http.post('http://localhost:8080/api/recaptcha', {response: response});
  }

  someData(){
    return "Some data";
  }

}
