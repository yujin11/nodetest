import {NgModule} from "@angular/core";
import {UserService} from "./user/user.service";
import {Dummy2Component} from "./dummy2.component/dummy2.component";

@NgModule({
  imports: [],
  providers: [
    UserService
  ],
  exports:[
    Dummy2Component
  ],
  declarations: [
    Dummy2Component
  ]
})
export class Shared2Module {

}
