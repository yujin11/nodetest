import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthorComponent} from './author/author.component';
import {Shared2Module} from "../shared2.module";
import {RouterModule} from "@angular/router"
import {routes} from "./author.routes";

@NgModule({
  declarations: [AuthorComponent],
  imports: [
    CommonModule,
    Shared2Module,
    RouterModule.forChild(routes),
  ],
})
export class AuthorModule {
}
