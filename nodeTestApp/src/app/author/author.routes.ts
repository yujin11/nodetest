import {Routes} from "@angular/router";
import {AuthorComponent} from "./author/author.component";

export const routes: Routes = [
  {
    path: '',
    component: AuthorComponent
  }
];
