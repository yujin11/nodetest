import {NgModule} from "@angular/core";
import {DummyComponent} from "./dummy.component/dummy.component";
import {UserService} from "./user/user.service";

@NgModule({
  imports: [],
  providers: [
    UserService
  ],
  exports: [
    DummyComponent
  ],
  declarations: [
    DummyComponent
  ]
})
export class ShareModule {

}
