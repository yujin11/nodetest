import {Component} from "@angular/core";
import {UserService} from "../../../user/user.service";
import {User} from "../../../user/models/User";
import {Observable} from "rxjs";
import {ApiService} from "../../../services/api.service";

@Component({
  selector: 'app-book',
  templateUrl: "./book.component.html"
})
export class BookComponent {

  userList$: Observable<User[]>;

  sortingModel;

  isAsc: boolean = true;
  public selectedSortingField: string = '';
  currentPage: number = 1;
  pageList: number;

  constructor(
    protected userService: UserService,
    protected apiService: ApiService
  ) {
    this.sortingModel = {
      name: "fName",
      lName: "lName",
      date: "created"
    };
    this.selectedSortingField = this.sortingModel.name;
    this.getList();
    this.userService.getUsersCount().subscribe(data => {
      this.pageList = data;
    })
  }

  private getList() {
    this.userList$ = this.userService.getUserList(this.isAsc, this.selectedSortingField, this.currentPage - 1);
    console.log(this.apiService.someData());
    console.log(this.userService.getSomeData())
  }

  deleteUser(userId: string) {
    this.userService.deleteUser(userId).subscribe(data => {
      this.getList();
    })
  }

  sort(field: string) {
    if (field == this.selectedSortingField) {
      this.isAsc = !this.isAsc;
    } else {
      this.isAsc = true;
    }
    this.selectedSortingField = field;
    this.getList();
  }


}
