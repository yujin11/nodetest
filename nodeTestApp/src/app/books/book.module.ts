import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {BookComponent} from "./components/user.component/book.component";
import {RouterModule} from "@angular/router";
import {routes} from "./book.routes";
import {ReactiveFormsModule} from "@angular/forms";
import {ShareModule} from "../share.module";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    ShareModule
  ],
  declarations: [BookComponent]
})
export class BookModule {

}
