import {Routes} from "@angular/router";
import {BookComponent} from "./components/user.component/book.component";

export const routes: Routes = [
  {
    path: '',
    component: BookComponent
  }
];
