import {Routes} from "@angular/router";
import {CarComponent} from "./car/car.component";

export const routes: Routes = [
  {
    path: '',
    component: CarComponent
  }
];
