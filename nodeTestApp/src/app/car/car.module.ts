import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CarComponent} from './car/car.component';
import {Shared2Module} from "../shared2.module";
import {RouterModule} from "@angular/router";
import {routes} from "./car.routes";

@NgModule({
  declarations: [CarComponent],
  imports: [
    CommonModule,
    Shared2Module,
    RouterModule.forChild(routes),
  ]
})
export class CarModule {
}
