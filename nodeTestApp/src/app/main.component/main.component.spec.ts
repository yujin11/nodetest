import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {MainComponent} from "./main.component";
import {NO_ERRORS_SCHEMA} from "@angular/core";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ReCaptchaV3Service} from "ng-recaptcha";
import {of} from "rxjs";

describe("Main Component", () => {
  let fixture: ComponentFixture<MainComponent>;
  let component: MainComponent;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [MainComponent],
      providers: [
        {
          provide: ReCaptchaV3Service, useValue: jasmine.createSpyObj('ReCaptchaV3Service', ['execute'])
        }
      ],
      imports: [
        HttpClientTestingModule
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('Component exist', () => {
    expect(component).toBeTruthy();
  });

  it('on Submit', () => {
    // spyOn(component, 'checkCaptcha').and.callFake(() => of(null));
    spyOn(component, 'checkCaptcha').and.returnValue(of(null));
    component.onSubmit();
    expect(component.checkCaptcha).toHaveBeenCalled();
  });

});
