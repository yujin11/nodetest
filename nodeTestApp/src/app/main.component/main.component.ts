import {Component} from "@angular/core";
import {ApiService} from "../services/api.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ReCaptchaV3Service} from "ng-recaptcha";
import {combineLatest, Observable, of} from "rxjs";
import {delay, flatMap, map, startWith} from "rxjs/operators";
import {UserService} from "../user/user.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html'
})
export class MainComponent {

  captchaForm: FormGroup;
  observables = [1, 5, 10].map(
    n => of(n).pipe(
      delay(n * 1000),   // emit 0 and then emit n after n seconds
      startWith(0),
    )
  );

  constructor(
    protected apiService: ApiService,
    protected userSerice: UserService,
    private recaptchaV3Service: ReCaptchaV3Service) {
    this.captchaForm = new FormGroup({
      name: new FormControl('', Validators.required),
      pass: new FormControl('', Validators.required)
    });
    this.getData();
    const combined = combineLatest(this.observables);
    combined.subscribe(value => console.log(value));
  }

  public getData() {
    this.apiService.getData().subscribe(data => {
      console.log('Data: ', data);
    });
    console.log(this.apiService.someData());
    console.log(this.userSerice.getSomeData());
  }

  public onSubmit() {
    console.log('On Submit.');
    this.checkCaptcha().subscribe((data) => {
      console.log('Captcha: ', data);
    })
  }

  checkCaptcha(): Observable<any> {
    return this.recaptchaV3Service.execute('homePage').pipe(flatMap((token: any) => {
      return this.apiService.sendReCaptcha(token)
    }));
  }

}
